let video;
let detector;
let detections = [];

function preload() {
  detector = ml5.objectDetector('cocossd');
}

function gotDetections(error, results) {
  if (error) {
    console.error(error);
  }
  detections = results;
  detector.detect(video, gotDetections);
}

function setup() {
  var cnv;
  cnv = createCanvas(200, 200);
  cnv.style('margin-left','27%');
  video = createCapture(VIDEO);
  video.size(800, 600);
  video.hide();
  detector.detect(video, gotDetections);
}

let n=0;

function draw() {
  image(video, 0, 0);
  let object;
  
  for (let i = 0; i < detections.length; i++) {
    let blok={confidence: 0.8631386160850525,
      height: 188.57468605041504,
      label: "block",
      normalized: {x: 0.48827028274536133, y: 0.6111004948616028, width: 0.4991105794906616, height: 0.392863929271698},
      width: 319.43077087402344,
      x: 312.49298095703125,
      y: 293.32823753356934}
    object = detections[i];

    if(object.label=="person"){
      //stroke(0, 255, 0);
      strokeWeight(2);
      noFill();
      rect(object.x, object.y, object.width, object.height);
      noStroke();
      fill(255);
      textSize(24);
      text(object.label, object.x + 10, object.y + 24);
      console.log(object)
    }

      //Screens

      if(object.label=="person" && object.width<=blok.width){
        if(n!=1){
          this.createCanvas(500, 350);
          console.log("screen1")
          n=1;
        }        
      }

      else if(object.label=="person" && object.width>blok.width){
        if(n!=2){
          n=2;
          this.createCanvas(700,500);
          console.log("screen2")
        }
      }

      else{
        if(n!=3){
          n=3;
          this.createCanvas(200, 200);
          console.log("screen3")
        }
      }
  }
}
